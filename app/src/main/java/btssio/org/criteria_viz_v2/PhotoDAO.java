package btssio.org.criteria_viz_v2;

import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

class PhotoDAO {

    private static String base = "BDResto";
    private static int version = 1;
    private BdSQLiteOpenHelper accesBD;

    public PhotoDAO(Context ct){
        accesBD = new BdSQLiteOpenHelper(ct, base, null, version);

    }

    public Photo getPhoto(int identifiant){
        Photo laPhoto = null;
        Cursor curseur;
        curseur = accesBD.getReadableDatabase().rawQuery("select * from photo where id_p="+identifiant+";",null);
        if (curseur.getCount() > 0) {
            curseur.moveToFirst();
            laPhoto = new Photo(identifiant, curseur.getString(1), new Resto().find(curseur.getInt(2)));
        }
        return laPhoto;
    }

    private ArrayList<Photo> cursorToPhotoArrayList(Cursor curseur){
        ArrayList<Photo> listePhotos = new ArrayList<>();
        int identifiant;
        String chemin;
        Resto leResto;

        curseur.moveToFirst();
        while (!curseur.isAfterLast()){
            identifiant= curseur.getInt(0);
            chemin = curseur.getString(1);
            leResto = new Resto().find(curseur.getInt(2));
            listePhotos.add(new Photo(identifiant,chemin,leResto));
            curseur.moveToNext();
        }

        return listePhotos;
    }

    public ArrayList<Photo> getPhotos(){
        Cursor curseur;
        String req = "select * from photo;";
        curseur = accesBD.getReadableDatabase().rawQuery(req,null);
        return cursorToPhotoArrayList(curseur);
    }
}
