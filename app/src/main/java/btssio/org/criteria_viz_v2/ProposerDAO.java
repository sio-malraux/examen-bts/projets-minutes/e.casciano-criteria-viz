package btssio.org.criteria_viz_v2;

import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

class ProposerDAO {

    private static String base = "BDResto";
    private static int version = 1;
    private BdSQLiteOpenHelper accesBD;

    public ProposerDAO(Context ct){
        accesBD = new BdSQLiteOpenHelper(ct, base, null, version);

    }

    public Proposer getPropose(Resto identifiant1, TypeCuisine identifiant2){
        Proposer lePropose = null;
        Cursor curseur;
        curseur = accesBD.getReadableDatabase().rawQuery("select * from Proposer where id_r="+identifiant1+" and id_tc="+identifiant2+";",null);
        if (curseur.getCount() > 0) {
            curseur.moveToFirst();
            lePropose = new Proposer(identifiant1, identifiant2);
        }
        return lePropose;
    }

    private ArrayList<Proposer> cursorToProposerArrayList(Cursor curseur){
        ArrayList<Proposer> listeProposes = new ArrayList<>();
        Resto leResto;
        TypeCuisine leTypeCuisine;

        curseur.moveToFirst();
        while (!curseur.isAfterLast()){
            leResto = new Resto().find(curseur.getInt(0));
            leTypeCuisine = new TypeCuisine().find(curseur.getInt(1));

            listeProposes.add(new Proposer(leResto, leTypeCuisine));
            curseur.moveToNext();
        }

        return listeProposes;
    }

    public ArrayList<Proposer> getProposes(){
        Cursor curseur;
        String req = "select * from proposer;";
        curseur = accesBD.getReadableDatabase().rawQuery(req,null);
        return cursorToProposerArrayList(curseur);
    }
}
