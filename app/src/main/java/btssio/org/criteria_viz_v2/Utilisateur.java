package btssio.org.criteria_viz_v2;

public class Utilisateur {
    private String mail;
    private String mdp;
    private String pseudo;
    private Utilisateur Utilisateur;

    public Utilisateur() {
        mail = null;
        mdp = "";
        pseudo = "";
    }

    public Utilisateur(String mail, String mdp, String pseudo) {
        this.mail = mail;
        this.mdp = mdp;
        this.pseudo = pseudo;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getMdp() {
        return mdp;
    }

    public void setMdp(String mdp) {
        this.mdp = mdp;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public Utilisateur find(String mail) {
        return this.Utilisateur;
    }

}
