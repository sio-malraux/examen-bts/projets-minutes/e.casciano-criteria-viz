package btssio.org.criteria_viz_v2;

import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

class TypeCuisineDAO {

    private static String base = "BDResto";
    private static int version = 1;
    private BdSQLiteOpenHelper accesBD;

    public TypeCuisineDAO(Context ct){
        accesBD = new BdSQLiteOpenHelper(ct, base, null, version);

    }

    public TypeCuisine getTypeCuisine(int identifiant){
        TypeCuisine leTypeCuisine = null;
        Cursor curseur;
        curseur = accesBD.getReadableDatabase().rawQuery("select * from type_cuisine where id_tc="+identifiant+";",null);
        if (curseur.getCount() > 0) {
            curseur.moveToFirst();
            leTypeCuisine = new TypeCuisine(identifiant, curseur.getString(1));
        }
        return leTypeCuisine;
    }

    private ArrayList<TypeCuisine> cursorToTypeCuisineArrayList(Cursor curseur){
        ArrayList<TypeCuisine> listeTypeCuisines = new ArrayList<>();
        int id;
        String libelle;

        curseur.moveToFirst();
        while (!curseur.isAfterLast()){
            id= curseur.getInt(0);
            libelle = curseur.getString(1);
            listeTypeCuisines.add(new TypeCuisine(id,libelle));
            curseur.moveToNext();
        }

        return listeTypeCuisines;
    }

    public ArrayList<TypeCuisine> getTypeCuisines(){
        Cursor curseur;
        String req = "select * from type_cuisine;";
        curseur = accesBD.getReadableDatabase().rawQuery(req,null);
        return cursorToTypeCuisineArrayList(curseur);
    }
}
