package btssio.org.criteria_viz_v2;

import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

class UtilisateurDAO {

    private static String base = "BDResto";
    private static int version = 1;
    private BdSQLiteOpenHelper accesBD;

    public UtilisateurDAO(Context ct){
        accesBD = new BdSQLiteOpenHelper(ct, base, null, version);

    }

    public Utilisateur getUtilisateur(String identifiant){
        Utilisateur lUtilisateur = null;
        Cursor curseur;
        curseur = accesBD.getReadableDatabase().rawQuery("select * from utilisateur where mail="+identifiant+";",null);
        if (curseur.getCount() > 0) {
            curseur.moveToFirst();
            lUtilisateur = new Utilisateur(identifiant, curseur.getString(1), curseur.getString(2));
        }
        return lUtilisateur;
    }

    private ArrayList<Utilisateur> cursorToUtilisateurArrayList(Cursor curseur){
        ArrayList<Utilisateur> listeUtilisateurs = new ArrayList<>();
        String mail, mdp, pseudo;

        curseur.moveToFirst();
        while (!curseur.isAfterLast()){
            mail= curseur.getString(0);
            mdp = curseur.getString(1);
            pseudo = curseur.getString(2);
            listeUtilisateurs.add(new Utilisateur(mail,mdp,pseudo));
            curseur.moveToNext();
        }

        return listeUtilisateurs;
    }

    public ArrayList<Utilisateur> getUtilisateurs(){
        Cursor curseur;
        String req = "select * from utilisateur;";
        curseur = accesBD.getReadableDatabase().rawQuery(req,null);
        return cursorToUtilisateurArrayList(curseur);
    }
}
