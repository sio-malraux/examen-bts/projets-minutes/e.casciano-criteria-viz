package btssio.org.criteria_viz_v2;

public class Critiquer {
    private Resto leResto;
    private Utilisateur lUtilisateur;
    private int note;
    private String commentaire;

    public Critiquer() {
        leResto = null;
        lUtilisateur = null;
        note = 0;
        commentaire = "";
    }

    public Critiquer(Resto leResto, Utilisateur lUtilisateur, int note, String commentaire) {
        this.leResto = leResto;
        this.lUtilisateur = lUtilisateur;
        this.note = note;
        this.commentaire = commentaire;
    }

    public int getNote() {
        return note;
    }

    public void setNote(int note) {
        this.note = note;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public Resto getLeResto() {
        return leResto;
    }

    public void setLeResto(Resto leResto) {
        this.leResto = leResto;
    }

    public Utilisateur getLUtilisateur() {
        return lUtilisateur;
    }

    public void setLUtilisateur(Utilisateur lUtilisateur) {
        this.lUtilisateur = lUtilisateur;
    }
}
