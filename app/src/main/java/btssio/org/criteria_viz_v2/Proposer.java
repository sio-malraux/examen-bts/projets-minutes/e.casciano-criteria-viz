package btssio.org.criteria_viz_v2;

public class Proposer {
    private Resto leResto;
    private TypeCuisine leTypeCuisine;

    public Proposer() {
        leResto = null;
        leTypeCuisine = null;
    }

    public Proposer(Resto leResto, TypeCuisine leTypeCuisine) {
        this.leResto = leResto;
        this.leTypeCuisine = leTypeCuisine;
    }

    public Resto getLeResto() {
        return leResto;
    }

    public void setLeResto(Resto leResto) {
        this.leResto = leResto;
    }

    public TypeCuisine getLeTypeCuisine() {
        return leTypeCuisine;
    }

    public void setLeTypeCuisine(TypeCuisine leTypeCuisine) {
        this.leTypeCuisine = leTypeCuisine;
    }
}

