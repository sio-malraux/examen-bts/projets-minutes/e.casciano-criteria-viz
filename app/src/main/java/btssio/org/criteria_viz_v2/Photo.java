package btssio.org.criteria_viz_v2;

public class Photo {
    private int idP;
    private String chemin;
    private Resto leResto;

    public Photo() {
        idP = 0;
        chemin = "";
        leResto = null;
    }

    public Photo(int idP, String chemin, Resto leResto) {
        this.idP = idP;
        this.chemin = chemin;
        this.leResto = leResto;
    }

    public int getIdP() {
        return idP;
    }

    public void setIdP(int idP) {
        this.idP = idP;
    }

    public String getChemin() {
        return chemin;
    }

    public void setChemin(String chemin) {
        this.chemin = chemin;
    }

    public Resto getLeResto() {
        return leResto;
    }

    public void setLeResto(Resto leResto) {
        this.leResto = leResto;
    }
}

