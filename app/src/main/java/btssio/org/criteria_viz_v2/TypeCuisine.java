package btssio.org.criteria_viz_v2;

public class TypeCuisine {
    private int idTc;
    private String libelle;
    private  TypeCuisine TypeCuisine;

    public TypeCuisine() {
        idTc = 0;
        libelle = "";
    }

    public TypeCuisine(int idTc, String libelle) {
        this.idTc = idTc;
        this.libelle = libelle;
    }

    public int getIdTc() {
        return idTc;
    }

    public void setIdTc(int idTc) {
        this.idTc = idTc;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public TypeCuisine find(int idTc) {
        return this.TypeCuisine;
    }
}
