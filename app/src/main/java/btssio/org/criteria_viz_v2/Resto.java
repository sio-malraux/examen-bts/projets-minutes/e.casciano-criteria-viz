package btssio.org.criteria_viz_v2;

public class Resto {
    private int id;
    private String nom;
    private String numAdr;
    private String voieAdr;
    private String cp;
    private String ville;
    private double latitude;
    private double longitude;
    private String descResto;
    private String horaires;
    private Resto Resto;

    public Resto() {
        id = 0;
        nom = "";
        numAdr = "";
        voieAdr = "";
        cp = "";
        ville = "";
        latitude = 0.0;
        longitude = 0.0;
        descResto = "";
        horaires = "";
    }

    public Resto(int id, String nom, String numAdr, String voieAdr, String cp, String ville, double latitude, double longitude, String descResto, String horaires) {
        this.id = id;
        this.nom = nom;
        this.numAdr = numAdr;
        this.voieAdr = voieAdr;
        this.cp = cp;
        this.ville = ville;
        this.latitude = latitude;
        this.longitude = longitude;
        this.descResto = descResto;
        this.horaires = horaires;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getNumAdr() {
        return numAdr;
    }

    public void setNumAdr(String numAdr) {
        this.numAdr = numAdr;
    }

    public String getVoieAdr() {
        return voieAdr;
    }

    public void setVoieAdr(String voieAdr) {
        this.voieAdr = voieAdr;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getDescResto() {
        return descResto;
    }

    public void setDescResto(String descResto) {
        this.descResto = descResto;
    }

    public String getHoraires() {
        return horaires;
    }

    public void setHoraires(String horaires) {
        this.horaires = horaires;
    }

    public Resto find(int id) {
        return this.Resto;
    }
}

