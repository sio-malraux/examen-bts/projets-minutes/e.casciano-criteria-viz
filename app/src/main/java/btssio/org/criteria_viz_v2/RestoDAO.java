package btssio.org.criteria_viz_v2;

import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

class RestoDAO {

    private static String base = "BDResto";
    private static int version = 1;
    private BdSQLiteOpenHelper accesBD;

    public RestoDAO(Context ct){
        accesBD = new BdSQLiteOpenHelper(ct, base, null, version);

    }

    public Resto getResto(int identifiant){
        Resto leResto = null;
        Cursor curseur;
        curseur = accesBD.getReadableDatabase().rawQuery("select * from resto where id_p="+identifiant+";",null);
        if (curseur.getCount() > 0) {
            curseur.moveToFirst();
            leResto = new Resto(identifiant, curseur.getString(1), curseur.getString(2), curseur.getString(3), curseur.getString(4),curseur.getString(5),
                    curseur.getDouble(6), curseur.getDouble(7), curseur.getString(8), curseur.getString(9));
        }
        return leResto;
    }

    private ArrayList<Resto> cursorToRestoArrayList(Cursor curseur){
        ArrayList<Resto> listeRestos = new ArrayList<>();
        int id;
        String nom;
        String numAdr;
        String voieAdr;
        String cp;
        String ville;
        double latitude;
        double longitude;
        String descResto;
        String horaires;

        curseur.moveToFirst();
        while (!curseur.isAfterLast()){
            id= curseur.getInt(0);
            nom = curseur.getString(1);
            numAdr = curseur.getString(2);
            voieAdr =curseur.getString(3);
            cp = curseur.getString(4);
            ville = curseur.getString(5);
            latitude = curseur.getDouble(6);
            longitude = curseur.getDouble(7);
            descResto = curseur.getString(8);
            horaires = curseur.getString(9);
            listeRestos.add(new Resto(id,nom,numAdr,voieAdr,cp,ville,latitude,longitude,descResto,horaires));
            curseur.moveToNext();
        }

        return listeRestos;
    }

    public ArrayList<Resto> getRestos(){
        Cursor curseur;
        String req = "select * from resto;";
        curseur = accesBD.getReadableDatabase().rawQuery(req,null);
        return cursorToRestoArrayList(curseur);
    }
}
