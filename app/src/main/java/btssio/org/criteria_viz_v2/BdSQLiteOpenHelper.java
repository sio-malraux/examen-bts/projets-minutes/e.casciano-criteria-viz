package btssio.org.criteria_viz_v2;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class BdSQLiteOpenHelper extends SQLiteOpenHelper {
    private String requete1 ="create table critiquer ("
            + "id_r integer PRIMARY KEY,"
            + "mail TEXT NOT NULL,"
            + "note integer DEFAULT NULL,"
            + "commentaire TEXT DEFAULT NULL);";

    private String requete2 = "CREATE TABLE photo ("
            + "id_p integer NOT NULL,"
            + "chemin TEXT DEFAULT NULL,"
            + "id_r integer DEFAULT NULL"
            + ");";

    private String requete3 =  "CREATE TABLE preferer ("
            + "mail TEXT NOT NULL ,"
            + "id_tc integer NOT NULL"
            + ");";

    private String requete4 = "CREATE TABLE proposer ("
            + "id_r integer NOT NULL,"
            + "id_tc integer NOT NULL"
            + ");";

    private String requete5 = "CREATE TABLE resto ("
            + "id integer NOT NULL,"
            + "nom TEXT  DEFAULT NULL,"
            + "num_adr TEXT DEFAULT NULL,"
            + "voie_adr TEXT DEFAULT NULL,"
            + "cp char DEFAULT NULL,"
            + "ville TEXT DEFAULT NULL,"
            + "latitude DOUBLE DEFAULT NULL,"
            + "longitude DOUBLE DEFAULT NULL,"
            + "desc_resto TEXT,"
            + "horaires TEXT);";

    private String requete6 = "CREATE TABLE type_cuisine ("
            + "id_tc integer NOT NULL,"
            + "libelle TEXT DEFAULT NULL);";

    private String requete7 = "CREATE TABLE utilisateur ("
            + "mail TEXT NOT NULL ,"
            + "mdp TEXT DEFAULT NULL ,"
            + "pseudo TEXT DEFAULT NULL);";


    public BdSQLiteOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(requete1);
        db.execSQL(requete2);
        db.execSQL(requete3);
        db.execSQL(requete4);
        db.execSQL(requete5);
        db.execSQL(requete6);
        db.execSQL(requete7);

        db.execSQL("INSERT INTO critiquer ( id_r , mail , note , commentaire ) VALUES (1 , 'jj. soueix@gmail.com', 3 , 'moyen'), " +
                "(1 , 'geraldine.taysse@gmail.com', 3 , 'Très bonne entrecote, les frites sont maisons et délicieuses.'), " +
                "(1 , 'nicolas.harispe@gmail . com', 4 , 'Très bon accueil.'), " +
                "(1 , 'test@bts .sio', 4 , '5/5 parce que j''aime les entrecotes'), " +
                "(1 , 'yann@lechambon.fr', 5 , NULL ), " +
                "(2 , 'jj. soueix@gmail.com', 2 , 'bof.'), " +
                "(2 , 'geraldine.taysse@gmail.com', 1 , 'À éviter ...'), " +
                "(2 , 'nicolas.harispe@gmail.com', 1, 'Cuisine très moyenne.'), " +
                "(2 , 'test@bts.sio', 5 , NULL ), " +
                "(4 , 'geraldine.taysse@gmail.com ', 5 , NULL ), " +
                "(4 , 'nicolas.harispe@gmail.com ', 5 , 'Rapide.'), " +
                "(5 , 'nicolas.harispe@gmail.com', 3 , 'Cuisine correcte.'), " +
                "(6 , 'nicolas.harispe@gmail.com', 4 , 'Cuisine de qualité.'), " +
                "(7 , 'alex.garat@gmail.com', 4, 'Bon accueil.'), " +
                "(7 , 'geraldine.taysse@gmail.com', NULL , NULL ), " +
                "(7 , 'nicolas.harispe@gmail.com', 5 , 'Excellent.'), " +
                "(8 , 'test@bts .sio', 1 , NULL ), " +
                "(8 , 'yann@lechambon.fr', 4 , NULL ), " +
                "(9 , 'geraldine.taysse@gmail.com', 4 , 'Très bon accueil :');");

        db.execSQL("INSERT INTO photo VALUES (0 , 'entrepote.jpg', 1) , (2 , 'sapporo.jpg', 3) , (3 , 'entrepote.jpg', 1) , (4 , 'barDuCharcutier.jpg', 2) , (6 , 'cidrerieDuFronton.jpg', 4) , " +
                "(7 , 'agadir.jpg ', 5) , (8 , 'leBistrotSainteCluque.jpg', 6) , (9 , 'auberge.jpg', 7) , (10 , 'laTableDePottoka.jpg', 8) , (11 , 'rotisserieDuRoyLeon.jpg', 9) , " +
                "(12 , 'barDuMarche.jpg', 10) , (13 , 'trinquetModerne.jpg', 11), (14 , 'cidrerieDuFronton2.jpg', 4) , (15 , 'cidrerieDuFronton3.jpg', 4);");

        db.execSQL("INSERT INTO preferer VALUES ('geraldine.taysse@gmail.com', 1) , ('geraldine.taysse@gmail.com', 5) , ('geraldine.taysse@gmail.com', 9) , ('geraldine.taysse@gmail.com', 10) , " +
                "('geraldine.taysse@gmail.com', 11) , ('michel.garay@gmail.com', 1) , ('michel.garay@gmail.com', 2) , ('michel.garay@gmail.com', 3) , ('michel.garay@gmail.com', 10) , " +
                "('nicolas.harispe@gmail.com', 1) , ('nicolas.harispe@gmail.com', 2) , ('nicolas.harispe@gmail.com', 11) , ('test@bts.sio', 1) , ('test@bts.sio', 2) , ('test@bts.sio', 3) , " +
                "('test@bts.sio', 10) , ('test@bts.sio', 11) , ('yann@lechambon.fr', 1) , ('yann@lechambon.fr', 7) , ('yann@lechambon.fr', 9) , ('yann@lechambon.fr', 10) , ('yann@lechambon.fr', 11);");

        db.execSQL("INSERT INTO proposer VALUES (1 , 1) , (2 , 1) , (3 , 3) , (4 , 1) , (4 , 8) , (4 , 11) , (5 , 3) , (6 , 10) , (7 , 6) , (8 , 11) , (9 , 10) , (10 , 1) , (11 , 1) , (11 , 10);");

        db.execSQL("INSERT INTO resto VALUES (1 , 'l''entrepote', '2', 'rue Maurice Ravel', '33000', 'Bordeaux', 44.7948 , -0.58754 , 'description', 'Ouverture - Midi : 11 h45 à 14 h30 Soir : de 18 h45 à 22 h30 - À emporter : de 11 h30 à 23h') , " +
                "(2 , 'le bar du charcutier', ' 30 ', 'rue Parlement Sainte - Catherine', '33000', 'Bordeaux', NULL , NULL , 'description', 'Ouverture - Midi : 11 h00 à 15 h30 Soir : de 17 h35 à 23 h30 - À emporter : de 11 h00 à 00 h00') , " +
                "(3 , 'Sapporo', '33', 'rue Saint Rémi', '33000', 'Bordeaux', NULL , NULL , 'Le Sapporo propose à ses clients de délicieux plats typiques japonais.', 'Ouverture - Midi : 11 h45 à 14 h30 Soir : de 18 h45 à 22 h30 - À emporter : de 11 h30 à 23h') , " +
                "(4 , 'Cidrerie du fronton', NULL , 'Place du Fronton', '64210', 'Arbonne', NULL , NULL , 'description', 'Ouverture - Midi : 11 h45 à 14 h30 Soir : de 18 h45 à 22 h30 - À emporter : de 11 h30 à 23h') , " +
                "(5 , 'Agadir', '3', 'Rue Sainte - Catherine', '64100', 'Bayonne', NULL , NULL , 'description', 'Ouverture - Midi : 11 h45 à 15 h30 Soir : de 18 h45 à 2 h00 - À emporter : de 12 h00 à 2h') , " +
                "(6 , 'Le Bistrot Sainte Cluque', '9', 'Rue Hugues', '64100', 'Bayonne', NULL , NULL , 'description', 'Ouverture - Midi : 11 h45 à 14 h30 Soir : de 18 h45 à 22 h30 - À emporter : de 11 h30 à 23h') , " +
                "(7 , 'la petite auberge', '15', 'rue des cordeliers', '64100', 'Bayonne', NULL , NULL , 'description', 'Ouverture - Midi : 11 h45 à 14 h30 Soir : de 18 h45 à 22 h30 - À emporter : de 11 h30 à 23h') , " +
                "(8 , 'La table de POTTOKA', '21', 'Quai Amiral Dubourdieu', '64100', 'Bayonne', NULL , NULL , 'description', 'Ouverture - Midi : 11 h15 à 14 h45 Soir : de 18 h00 à 23 h45 - À emporter : de 11 h00 à 23 h30') , " +
                "(9 , 'La Rotisserie du Roy Léon', '8', 'rue de coursic', '64100', 'Bayonne', NULL , NULL , 'description', 'Ouverture - Midi : 11 h45 à 15 h30 Soir : de 19 h00 à 21 h30 - À emporter : de 11 h30 à 22h') , " +
                "(10 , 'Bar du March ´e', '39 ', 'Rue des Basques ', '64100 ', 'Bayonne ', NULL , NULL , 'description', 'Ouverture - Midi : 11 h15 à 13 h30 Soir : de 18 h30 à 21 h00 - À emporter : de 11 h00 à 21h') , " +
                "(11 , 'Trinquet Moderne', '60', 'Avenue Dubrocq', '64100', 'Bayonne', NULL , NULL , 'description', 'Ouverture - Midi : 12 h15 à 16 h30 Soir : de 19h à 2 h30 - À emporter : de 11 h30 à 23 h55');");

        db.execSQL("INSERT INTO type_cuisine VALUES (1 , 'sud ouest') , (2 , 'japonaise') , (3 , 'orientale') , (4 , 'fastfood') , (5 , 'vegetarienne') , (6 , 'vegan') , (7 , 'crepe') , (8 , 'sandwich') , " +
                "(9 , 'tartes') , (10 , 'viande') , (11 , 'grillade');");

        db.execSQL("INSERT INTO utilisateur VALUES ('alex.garat@gmail.com', '$1$zvN5hYSQSQDFUIQSdufUQSDFznHF5osT.', '@lex') , ('jj.soueix@gmail.com', '$1$zvN5hYMI$SDFGSDFGJqJSDJF.', 'drskott') , " +
                "('geraldine.taysse@gmail.com', 'seSzpoUAQgIl.', 'pich') , ('michel.garay@gmail.com', '$1$zvN5hYMI$VSatLQ6SDFGdsfgznHF5osT.', 'Mitch') , " +
                "('nicolas.harispe@gmail.com', '$1$zvNDSFQSdfqsDfQsdfsT.', 'Nico40') , ('test@bts.sio', 'seSzpoUAQgIl.', 'testeur SIO') , ('yann@lechambon.fr', 'sej6dETYl/ea.', 'yann');");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //db.execSQL("DROP TABLE Client");
        //onCreate(db);
    }

}
