package btssio.org.criteria_viz_v2;

public class Preferer {
    private Utilisateur lUtilisateur;
    private TypeCuisine leTypeCuisine;

    public Preferer() {
        lUtilisateur = null;
        leTypeCuisine = null;
    }

    public Preferer(Utilisateur lUtilisateur, TypeCuisine leTypeCuisine) {
        this.lUtilisateur = lUtilisateur;
        this.leTypeCuisine = leTypeCuisine;
    }

    public Utilisateur getLUtilisateur() {
        return lUtilisateur;
    }

    public void setLUtilisateur(Utilisateur lUtilisateur) {
        this.lUtilisateur = lUtilisateur;
    }

    public TypeCuisine getLeTypeCuisine() {
        return leTypeCuisine;
    }

    public void setLeTypeCuisine(TypeCuisine leTypeCuisine) {
        this.leTypeCuisine = leTypeCuisine;
    }
}

