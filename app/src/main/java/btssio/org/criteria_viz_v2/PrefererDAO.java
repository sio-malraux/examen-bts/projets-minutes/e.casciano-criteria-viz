package btssio.org.criteria_viz_v2;

import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

class PrefererDAO {

    private static String base = "BDResto";
    private static int version = 1;
    private BdSQLiteOpenHelper accesBD;

    public PrefererDAO(Context ct){
        accesBD = new BdSQLiteOpenHelper(ct, base, null, version);

    }

    public Preferer getPrefere(Utilisateur identifiant1, TypeCuisine identifiant2){
        Preferer lePrefere = null;
        Cursor curseur;
        curseur = accesBD.getReadableDatabase().rawQuery("select * from preferer where mail="+identifiant1+" and id_tc="+identifiant2+";",null);
        if (curseur.getCount() > 0) {
            curseur.moveToFirst();
            lePrefere = new Preferer(identifiant1, identifiant2);
        }
        return lePrefere;
    }

    private ArrayList<Preferer> cursorToPrefererArrayList(Cursor curseur){
        ArrayList<Preferer> listePreferes = new ArrayList<>();
        Utilisateur lUtilisateur;
        TypeCuisine leTypeCusine;

        curseur.moveToFirst();
        while (!curseur.isAfterLast()){
            lUtilisateur= new Utilisateur().find(curseur.getString(0));
            leTypeCusine = new TypeCuisine().find(curseur.getInt(1));
            listePreferes.add(new Preferer(lUtilisateur,leTypeCusine));
            curseur.moveToNext();
        }

        return listePreferes;
    }

    public ArrayList<Preferer> getPreferes(){
        Cursor curseur;
        String req = "select * from preferer;";
        curseur = accesBD.getReadableDatabase().rawQuery(req,null);
        return cursorToPrefererArrayList(curseur);
    }
}
