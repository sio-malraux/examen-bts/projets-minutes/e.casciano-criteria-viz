package btssio.org.criteria_viz_v2;

import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

class CritiquerDAO {

    private static String base = "BDResto";
    private static int version = 1;
    private BdSQLiteOpenHelper accesBD;

    public CritiquerDAO(Context ct){
        accesBD = new BdSQLiteOpenHelper(ct, base, null, version);

    }

    public Critiquer getCritique(Resto identifiant1, Utilisateur identifiant2){
        Critiquer laCritique = null;
        Cursor curseur;
        curseur = accesBD.getReadableDatabase().rawQuery("select * from critiquer where id_r="+identifiant1+" and mail="+identifiant2+";",null);
        if (curseur.getCount() > 0) {
            curseur.moveToFirst();
            laCritique = new Critiquer(identifiant1, identifiant2, curseur.getInt(2), curseur.getString(3));
        }
        return laCritique;
    }

    private ArrayList<Critiquer> cursorToCritiquerArrayList(Cursor curseur){
        ArrayList<Critiquer> listeCritiques = new ArrayList<>();
        Resto leResto;
        Utilisateur lUtilisateur;
        int note;
        String commentaire;

        curseur.moveToFirst();
        while (!curseur.isAfterLast()){
            leResto = new Resto().find(curseur.getInt(0));
            lUtilisateur = new Utilisateur().find(curseur.getString(1));
            note = curseur.getInt(2);
            commentaire = curseur.getString(3);
            listeCritiques.add(new Critiquer(leResto,lUtilisateur,note,commentaire));
            curseur.moveToNext();
        }

        return listeCritiques;
    }

    public ArrayList<Critiquer> getCritiques(){
        Cursor curseur;
        String req = "select * from critiquer;";
        curseur = accesBD.getReadableDatabase().rawQuery(req,null);
        return cursorToCritiquerArrayList(curseur);
    }
}
